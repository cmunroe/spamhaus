<?php

namespace spamhaus;

/**
 * spamhaus edrop file.
 * 
 * EDROP is an extension of the DROP list that includes 
 * suballocated netblocks controlled by spammers or cyber 
 * criminals. EDROP is meant to be used in addition to
 * the direct allocations on the DROP list.
 */
class edrop extends core{

    protected $name = 'edrop.txt';

}