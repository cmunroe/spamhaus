<?php

namespace spamhaus;

/**
 * spamhaus drop list.
 * 
 * The DROP list will not include any IP address space under 
 * the control of any legitimate network - even if being 
 * used by "the spammers from hell". DROP will only include 
 * netblocks allocated directly by an established Regional 
 * Internet Registry (RIR) or National Internet Registry (NIR) 
 * such as ARIN, RIPE, AFRINIC, APNIC, LACNIC or KRNIC or 
 * direct RIR allocations.
 */
class drop extends core {

    protected $name = 'drop.txt';
    
}