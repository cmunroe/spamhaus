<?php

namespace spamhaus;

/**
 * spamhaus asndrop file.
 * 
 * ASN-DROP contains a list of Autonomous System Numbers 
 * controlled by spammers or cyber criminals, as well as 
 * "hijacked" ASNs. ASN-DROP can be used to filter BGP 
 * routes which are being used for malicious purposes. 
 */
class asnDrop extends core{

    protected $name = 'asndrop.txt';

    /**
     * Parse the comments of the asndrop file into easily
     * usable array keys countryCode and asName.
     *
     * @return this chain. Will return false if data is broken.
     */
    public function parseCommentsASN(){

        if($this->list === false){

            return false;
            
        }

        foreach($this->list as $key => $item){


            $comment = explode("|", $item['comment']);

            if(trim($comment[0]) !== null){

                $this->list[$key]['countryCode'] = trim($comment[0]);

            }

            if(trim($comment[1]) !== null){

                $this->list[$key]['asName'] = trim($comment[1]);

            }

        }

        return $this;

    }

}