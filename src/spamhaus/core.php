<?php

namespace spamhaus;

/**
 * Core function for manipulating spamhaus txt drop files. 
 */
class core {

    private $mainURL = 'https://www.spamhaus.org/drop/';
    protected $download = false;
    protected $list = false;
    protected $fileComments = false;

    /**
     * Grab the text file from the spamhaus website.
     * This data is cached and can be manipulated.
     *
     * @return this chain. Will return false if download failed.
     */
    public function getFile(){

        $file = file_get_contents($this->mainURL . $this->name);

        // Check to make sure we have a valid request.
        if (!strpos($http_response_header[0], "200")) {

            // Something went wrong, let's exit.
            $this->download = false;
            return false;
    
        }

        $this->download = $file;

        return $this;

    }

    /**
     * Parse the text file into an array.
     *
     * @return this chain. Will return false if download
     * failed or doesn't exist.
     */
    public function parser(){

        // Make sure we have a downloaded file.
        if($this->download == false){

            return false;

        }

        // Explode the list line by line.

        $file = explode("\n", $this->download);

        $this->list = array();

        foreach($file as $line){

            $lineData = explode(';', $line);

            // See if our first bit of data is a comment.
            if(array_key_exists(0, $lineData) && trim($lineData[0]) == null){

                // Append to comment.
                $this->fileComments .= $lineData[1] . "\n";

            }

            // Parse the actual data bits.
            else{

                $block = false;
                $comment = false;

                if(array_key_exists(0, $lineData)){

                    $block = trim($lineData[0]);

                }

                if(array_key_exists(1, $lineData)){

                    $comment = trim($lineData[1]);

                }


                if($block !== false){

                    $this->list[] = array(

                        'block' => $block,
                        'comment' => $comment

                    );

                }

            }

        }

        return $this;

    }


    /**
     * For ip lists this will find ASN info of that ip address.
     * Using the ip2asn.ipinfo.app api. Some IPs are not announced.
     * 
     *
     * @return this chain.
     */
    public function ip2asn(){

        $ip2asn = new \ip2asn\core;

        if($this->name == 'asndrop.txt'){

            return $this;

        }

        foreach($this->list as $key => $item){

            
            $this->list[$key]['asn'] = array_unique($ip2asn->get($item['block'])->ASNs());


        }

        return $this;

    }

    /**
     * Dump our finished list.
     *
     * @return array parsed list.
     */
    public function dump(){

        return $this->list;

    }

    /**
     * Return comments from the top of the spamhaus text file.
     *
     * @return string comments.
     */
    public function comments(){

        return $this->fileComments;

    }

}