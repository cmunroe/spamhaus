<?php

namespace spamhaus;

/**
 * spamhaus dropv6 file.
 * 
 * The DROPv6 list includes IPv6 ranges allocated to 
 * spammers or cyber criminals. DROPv6 will only 
 * include IPv6 netblocks allocated directly by an 
 * established Regional Internet Registry (RIR) or 
 * National Internet Registry (NIR) such as ARIN, 
 * RIPE, AFRINIC, APNIC, LACNIC or KRNIC or direct 
 * RIR allocations.
 */
class dropv6 extends core{

    protected $name = 'dropv6.txt';
  
}